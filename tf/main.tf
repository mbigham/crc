terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.62.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.33.1"
    }
  }
  required_version = ">= 1.1.9"
}

provider "aws" {
  region = var.aws_region
}

module "s3-bucket" {
  source                   = "terraform-aws-modules/s3-bucket/aws"
  version                  = "3.8.2"
  acl                      = "public-read"
  control_object_ownership = true
  force_destroy            = true
  bucket                   = var.root_domain_bucket_name #cloudresumechallenge-masonbigham.com-prod-bucket
  website = {
    index_document = "index.html"
    error_document = "404.html"
  }
  tags = {
    Project   = "cloudresumechallenge"
    ManagedBy = "terraform"
  }
}

resource "aws_s3_object" "index_html" {
  bucket       = var.root_domain_bucket_name
  key          = "index.html"
  source       = "../resume/index.html"
  etag         = filemd5("../resume/index.html")
  content_type = "text/html"
  acl          = "public-read"
  depends_on = [
    module.s3-bucket
  ]
}

resource "aws_s3_object" "resume_css" {
  bucket              = var.root_domain_bucket_name
  key                 = "resume.css"
  source              = "../resume/resume.css"
  etag                = filemd5("../resume/resume.css")
  content_type        = "text/css"
  content_disposition = "text/css"
  acl                 = "public-read"
  depends_on = [
    module.s3-bucket
  ]
}

resource "aws_s3_object" "reset-fonts-grids_css" {
  bucket              = var.root_domain_bucket_name
  key                 = "reset-fonts-grids.css"
  source              = "../resume/reset-fonts-grids.css"
  etag                = filemd5("../resume/reset-fonts-grids.css")
  content_type        = "text/css"
  content_disposition = "text/css"
  acl                 = "public-read"
  depends_on = [
    module.s3-bucket
  ]
}

resource "aws_s3_object" "notfound_html" {
  bucket       = var.root_domain_bucket_name
  key          = "404.html"
  source       = "../resume/404.html"
  etag         = filemd5("../resume/404.html")
  content_type = "text/html"
  acl          = "public-read"
  depends_on = [
    module.s3-bucket
  ]
}

module "cloudfront_cloudflare_dns_ssl" {
  source = "./modules/cloudfront_cloudflare_dns_ssl"

  root_domain_name               = var.root_domain_name
  aws_s3_bucket_website_endpoint = module.s3-bucket.s3_bucket_website_endpoint
  aws_s3_bucket_id               = module.s3-bucket.s3_bucket_id
  aws_region                     = var.aws_region
}

