terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.62.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.33.1"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

provider "cloudflare" {
}

resource "aws_acm_certificate" "cert" {
  domain_name               = var.root_domain_name
  subject_alternative_names = ["*.${var.root_domain_name}"]
  validation_method         = "DNS"

  tags = {
    Name = var.root_domain_name
  }
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = aws_acm_certificate.cert.arn
}

data "cloudflare_zones" "domain" {
  filter {
    name = "masonbigham.com"
  }
}

resource "cloudflare_record" "acm" {
  zone_id = data.cloudflare_zones.domain.zones[0].id

  // Cloudflare doesn't support `allow_overwrite` field like the route53_record 
  // resource; as a result, this configuration hardcodes the first record to 
  // verify the ACM certificate.
  // for_each = {
  //   for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
  //     name   = dvo.resource_record_name
  //     record = dvo.resource_record_value
  //     type   = dvo.resource_record_type
  //   }
  // }

  name  = aws_acm_certificate.cert.domain_validation_options.*.resource_record_name[0]
  type  = aws_acm_certificate.cert.domain_validation_options.*.resource_record_type[0]
  value = trimsuffix(aws_acm_certificate.cert.domain_validation_options.*.resource_record_value[0], ".")

  // Must be set to false. ACM validation false otherwise
  proxied = false
}

// This configuration uses Cloudfront defaults
// Cloudfront is required for static site hosting with S3 if bucket name is
// already taken.
resource "aws_cloudfront_distribution" "dist" {
  origin {
    domain_name = var.aws_s3_bucket_website_endpoint
    origin_id   = var.aws_s3_bucket_id
    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }
  enabled             = true
  default_root_object = "index.html"

  aliases = [
    var.root_domain_name, "www.${var.root_domain_name}"
  ]

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.aws_s3_bucket_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 300
    max_ttl                = 300
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate_validation.cert.certificate_arn
    ssl_support_method  = "sni-only"
  }
}

resource "cloudflare_record" "site_cname" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = var.root_domain_name
  value   = aws_cloudfront_distribution.dist.domain_name
  type    = "CNAME"

  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "www" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = "www"
  value   = aws_cloudfront_distribution.dist.domain_name
  type    = "CNAME"

  ttl     = 1
  proxied = true
}

resource "cloudflare_page_rule" "https" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  target  = "*${var.root_domain_name}/*"
  actions {
    always_use_https = true
  }
}

