output "cloudfront_endpoint" {
  description = "Cloudfront endpoint"
  value       = aws_cloudfront_distribution.dist.domain_name
}

output "domain_name" {
  description = "Website endpoint"
  value       = var.root_domain_name
}
