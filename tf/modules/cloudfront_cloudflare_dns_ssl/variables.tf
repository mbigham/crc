variable "root_domain_name" {
  default = "masonbigham.com"
}

variable "aws_s3_bucket_website_endpoint" {
  default = "default_bucket_website_endpoint"
}

variable "aws_s3_bucket_id" {
  default = "default_bucket_id"
}

variable "aws_region" {
  default = "us-east-1"
}

