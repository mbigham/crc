env                     = "prod"
root_domain_name        = "masonbigham.com"
root_domain_bucket_name = "crc-masonbigham.com-prod-bucket"
aws_region              = "us-east-1"
