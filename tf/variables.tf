variable "env" {
  description = "The environment for the infrastructure"
  type        = string
}

variable "root_domain_name" {
  description = "The root domain name for the website"
  type        = string
}

variable "root_domain_bucket_name" {
  description = "The S3 bucket name for the root domain"
  type        = string
}

variable "aws_region" {
  description = "The AWS region to deploy the infrastructure"
  type        = string
}

